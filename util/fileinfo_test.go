package util

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/hokanio/notify/event"
)

func TestChecksum(t *testing.T) {
	tests := []struct {
		name     string
		filePath string
		want     string
		wantErr  bool
	}{
		{
			name:     "case 1",
			filePath: "testdata/test.txt",
			want:     "06ae84e5d26e5537ee9b7b732fb2c091f72884b920102e5ecf3f2b13a6dd1933",
			wantErr:  false,
		},
		{
			name:     "case 2",
			filePath: "testdata/wrong.txt",
			wantErr:  true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := Checksum(tt.filePath)
			if tt.wantErr {
				assert.Error(t, err)
			} else {
				assert.Equal(t, tt.want, got)
			}
		})
	}
}

func TestContentType(t *testing.T) {
	tests := []struct {
		name     string
		filePath string
		want     string
		wantErr  bool
	}{
		{
			name:     "case 1",
			filePath: "testdata/test.txt",
			want:     "application/octet-stream",
			wantErr:  false,
		},
		{
			name:     "case 2",
			filePath: "testdata/wrong.txt",
			wantErr:  true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := ContentType(tt.filePath)
			if tt.wantErr {
				assert.Error(t, err)
			} else {
				assert.Equal(t, tt.want, got)
			}
		})
	}
}

func Test_checkValidFile(t *testing.T) {
	tests := []struct {
		name             string
		absoluteFilePath string
		action           event.ActionType
		want             bool
	}{
		{
			name:             "file deleted",
			absoluteFilePath: "/some/file",
			action:           event.FileRemoved,
			want:             true,
		},
		{
			name:             "file renamed",
			absoluteFilePath: "/some/file",
			action:           event.FileRenamedOldName,
			want:             true,
		},
		{
			name:             "file is dir",
			absoluteFilePath: "testdata/",
			action:           event.FileAdded,
			want:             false,
		},
		{
			name:             "file is ok",
			absoluteFilePath: "testdata/test.txt",
			action:           event.FileAdded,
			want:             true,
		},
		{
			name:             "file not there",
			absoluteFilePath: "testdata/wrong.txt",
			action:           event.FileAdded,
			want:             false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got := CheckValidFile(tt.absoluteFilePath, tt.action)
			assert.Equal(t, tt.want, got)
		})
	}
}
