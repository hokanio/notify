package watcher

import (
	"context"
	"fmt"
	"os"
	"path/filepath"
	"sync"
	"time"

	"gitlab.com/hokanio/notify/event"
)

var globalIgnoredFiles []string = []string{".crdownload"}

// DirectoryWatcher represents a configuration for a single directory to watch
type DirectoryWatcher struct {
	events chan event.Event
	log    chan event.Log

	event.Waiter
}

// Options represents global options for the notify
type Options struct {
	IgnoreFiles []string
}

// Callback holds information about watcher channels
type Callback struct {
	Stop  bool
	Pause bool
}

var watcher *DirectoryWatcher
var once sync.Once

var watchersCallbackMutex sync.Mutex
var watchersCallback = make(map[string]chan Callback)

// ActionToString maps Action value to string
func ActionToString(action event.ActionType) string {
	switch action {
	case event.FileAdded:
		return "added"
	case event.FileRemoved:
		return "removed"
	case event.FileModified:
		return "modified"
	case event.FileRenamedOldName:
		return "renamedFrom"
	case event.FileRenamedNewName:
		return "renamedTo"
	case event.FileExists:
		return "exists"
	default:
		return "invalid"
	}
}

func RegisterCallback(path string) chan Callback {
	watchersCallbackMutex.Lock()
	defer watchersCallbackMutex.Unlock()
	cb := make(chan Callback)
	watchersCallback[path] = cb
	return cb
}

func UnregisterCallback(path string) {
	watchersCallbackMutex.Lock()
	defer watchersCallbackMutex.Unlock()
	delete(watchersCallback, path)
}

func LookupForCallback(path string) (chan Callback, bool) {
	watchersCallbackMutex.Lock()
	defer watchersCallbackMutex.Unlock()
	data, ok := watchersCallback[path]
	return data, ok
}

// Create new global instance of file watcher
func Create(ctx context.Context, callbackCh chan event.Event, logCh chan event.Log, options *Options) *DirectoryWatcher {
	once.Do(func() {
		go processContext(ctx)
		watcher = &DirectoryWatcher{
			events: callbackCh,
			log:    logCh,

			Waiter: event.Waiter{
				IgnoreFiles: append(globalIgnoredFiles, options.IgnoreFiles...),
				EventCh:     callbackCh,
				Timeout:     1 * time.Second,
				MaxCount:    5,
			},
		}
	})
	return watcher
}

func (w *DirectoryWatcher) Event() chan event.Event {
	return w.events
}

func (w *DirectoryWatcher) Log() chan event.Log {
	return w.log
}

func (w *DirectoryWatcher) scan(path string) error {
	path = filepath.Clean(path)
	fileDebug("DEBUG", fmt.Sprintf("scan(): starting recursive scanning from root [%q]", path))
	return filepath.Walk(path, func(absoluteFilePath string, fileInfo os.FileInfo, err error) error {
		if fileInfo.IsDir() {
			dir := fileInfo.Name()
			if ignoreFolders[dir] {
				fileDebug("DEBUG", fmt.Sprintf("dir [%s] is excluded from watching", absoluteFilePath))
				return filepath.SkipDir
			}
			if os.IsPermission(err) {
				fileDebug("DEBUG", fmt.Sprintf("dir [%s] is excluded from watching because of an error: %v", absoluteFilePath, err))
				return filepath.SkipDir
			}
		}
		if err != nil {
			fileError("ERROR", fmt.Errorf("can't scan [%s]: %v", path, err))
			return filepath.SkipDir
		}
		if !fileInfo.IsDir() {
			fileChangeNotifier(absoluteFilePath, event.FileExists, &event.AdditionalInfo{
				Size:    fileInfo.Size(),
				ModTime: fileInfo.ModTime(),
			})
		}
		return nil
	})
}

func (w *DirectoryWatcher) RescanAll() {
	fileDebug("DEBUG", "RescanAll(): event triggerd")
	for path := range watchersCallback {
		err := w.scan(path)
		if err != nil {
			fileError("CRITICAL", fmt.Errorf("cannot scan directory [%s]", path))
		}
	}
}

func processContext(ctx context.Context) {
	<-ctx.Done()
	watchersCallbackMutex.Lock()
	defer watchersCallbackMutex.Unlock()
	for _, ch := range watchersCallback {
		ch <- Callback{
			Stop: true,
		}
	}
}

// StopWatching sends a signal to stop watching a directory
func (w *DirectoryWatcher) StopWatching(watchDirectoryPath string) {
	ch, ok := LookupForCallback(watchDirectoryPath)
	if ok {
		ch <- Callback{
			Stop:  true,
			Pause: false,
		}
		UnregisterCallback(watchDirectoryPath)
	}
}

func fileError(lvl string, err error) {
	watcher.log <- event.FormatLogMessage(lvl, err.Error())
}

func fileDebug(lvl string, msg string) {
	watcher.log <- event.FormatLogMessage(lvl, msg)
}

func fileChangeNotifier(absoluteFilePath string, action event.ActionType, info *event.AdditionalInfo) {
	data := event.Event{
		Path:       absoluteFilePath,
		Action:     action,
		ActionName: ActionToString(action),
	}

	var key string
	switch action {
	case event.FileExists:
		watcher.EventCh <- data
		return
	case event.FileRenamedOldName:
		key = watcher.GetRenameHandler()
	case event.FileRenamedNewName:
		key = watcher.GetRenameHandler()
	default:
		key = absoluteFilePath
	}

	// if notification is registered for this file, send the event
	waitChan, exists := watcher.LookupForFileNotification(key)
	if exists {
		waitChan <- data
		return
	}

	// register new file notification channel
	waitChan = watcher.RegisterFileNotification(key)
	go watcher.Wait(waitChan)
	waitChan <- data
}
