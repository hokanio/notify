package watcher

import (
	"fmt"
	"os"
	"path/filepath"
	"runtime"
	"unsafe"

	"golang.org/x/sys/windows"

	"gitlab.com/hokanio/notify/core"
	"gitlab.com/hokanio/notify/event"
	"gitlab.com/hokanio/notify/util"
)

// exclude these folders from the recursive scan
var ignoreFolders = map[string]bool{
	// $ sign indicates that the folder is hidden
	`$Recycle.Bin`: true,
	`$WinREAgent`:  true,
	`$SysReset`:    true,
}

// StartWatching starts a CGO function for getting the notifications
func (w *DirectoryWatcher) StartWatching(path string, options *core.WatchingOptions) {
	if _, err := os.Stat(path); os.IsNotExist(err) {
		fileError("CRITICAL", fmt.Errorf("can't start watching [%s]: %v", path, err))
		return
	}

	_, found := LookupForCallback(path)
	if found {
		fileDebug("INFO", fmt.Sprintf("directory [%s] is already watched", path))
		return
	}

	fileDebug("INFO", fmt.Sprintf("start watching [%s]", path))

	// ch := RegisterCallback(path)
	// go func() {
	// 	for p := range ch {
	// 		if p.Stop {
	// 			C.StopWatching(cpath)
	// 		}
	// 	}
	// }()

	if options.Rescan {
		err := w.scan(path)
		if err != nil {
			fileError("CRITICAL", fmt.Errorf("can't scan [%s]: %v", path, err))
			return
		}
	}

	go watchDirectory(path)
}

func notify(path, file string, action event.ActionType) {
	absoluteFilePath := filepath.Join(path, file)

	if util.CheckValidFile(absoluteFilePath, action) {
		fileChangeNotifier(absoluteFilePath, action, nil)
	}
}

// handle defines host specific observer properties.
type handle struct {
	windows.Handle
}

func open(directory string) (*handle, error) {
	cwd, err := windows.UTF16PtrFromString(directory)
	if err != nil {
		return nil, err
	}
	h, err := windows.CreateFile(cwd,
		windows.FILE_LIST_DIRECTORY,
		windows.FILE_SHARE_READ|windows.FILE_SHARE_WRITE|windows.FILE_SHARE_DELETE,
		nil,
		windows.OPEN_EXISTING,
		windows.FILE_FLAG_BACKUP_SEMANTICS,
		0,
	)
	if err != nil {
		return nil, fmt.Errorf("CreateFile %w", err)
	}

	return &handle{h}, nil
}

// watchDirectory events and notify observer's callbacks.
func watchDirectory(dir string) {
	runtime.LockOSThread() // tie this goroutine to an OS thread
	defer runtime.UnlockOSThread()

	filter := uint32(windows.FILE_NOTIFY_CHANGE_LAST_WRITE |
		windows.FILE_NOTIFY_CHANGE_SIZE |
		windows.FILE_NOTIFY_CHANGE_ATTRIBUTES |
		windows.FILE_NOTIFY_CHANGE_FILE_NAME |
		windows.FILE_NOTIFY_CHANGE_DIR_NAME)

	handle, err := open(dir)
	if err != nil {
		panic(err)
	}

	for {
		events := make([]byte, 65536)
		var n uint32
		if err := windows.ReadDirectoryChanges(
			handle.Handle,
			&events[0],
			uint32(len(events)),
			true,
			filter,
			&n,
			nil,
			0,
		); err != nil {
			windows.Close(handle.Handle)
			fmt.Printf("ReadDirectoryChanges: %v\n", err)
			// errorChan <- core.Error("ReadDirectoryChanges", err)
			return
		}

		var e *windows.FileNotifyInformation
		for i := 0; i < int(n); i += int(e.NextEntryOffset) {
			e = (*windows.FileNotifyInformation)(unsafe.Pointer(&events[i]))
			name := windows.UTF16ToString((*[1024]uint16)(unsafe.Pointer(&e.FileName))[:e.FileNameLength/2])

			switch e.Action {
			case windows.FILE_ACTION_ADDED:
				// fmt.Printf("FILE_ACTION_ADDED: %q\n", name)
				notify(dir, name, event.FileAdded)
			case windows.FILE_ACTION_RENAMED_NEW_NAME:
				// fmt.Printf("FILE_ACTION_RENAMED_NEW_NAME: %q\n", name)
				notify(dir, name, event.FileRenamedNewName)
			case windows.FILE_ACTION_MODIFIED:
				// fmt.Printf("FILE_ACTION_MODIFIED: %q\n", name)
				notify(dir, name, event.FileModified)

			case windows.FILE_ACTION_RENAMED_OLD_NAME:
				// fmt.Printf("FILE_ACTION_RENAMED_OLD_NAME: %q\n", name)
				notify(dir, name, event.FileRenamedOldName)

			case windows.FILE_ACTION_REMOVED:
				// fmt.Printf("FILE_ACTION_REMOVED: %q\n", name)
				notify(dir, name, event.FileRemoved)
			}

			if e.NextEntryOffset == 0 {
				break
			}
		}
	}
}
