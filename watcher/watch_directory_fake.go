// +build fake

package watcher

import (
	"time"

	"gitlab.com/hokanio/notify/core"
	"gitlab.com/hokanio/notify/event"
)

var ignoreFolders = map[string]bool{}

func (i *DirectoryWatcher) StartWatching(root string, _ *core.WatchingOptions) {
	time.Sleep(time.Second)
	fileChangeNotifier(root+"/test.txt", event.FileAdded, nil)
}
