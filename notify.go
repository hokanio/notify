package notify

import (
	"context"

	"gitlab.com/hokanio/notify/core"
	"gitlab.com/hokanio/notify/event"
	"gitlab.com/hokanio/notify/watcher"
)

// Setup returns a channel for file change notifications and errors
func Setup(ctx context.Context, options *watcher.Options) core.DirectoryWatcher {
	eventCh := make(chan event.Event)
	logCh := make(chan event.Log)

	return watcher.Create(ctx, eventCh, logCh, options)
}
