package main

import (
	"context"
	"log"
	"os"
	"runtime"

	"gitlab.com/hokanio/notify"
	"gitlab.com/hokanio/notify/core"
	"gitlab.com/hokanio/notify/watcher"
)

var dirsWin = []string{"C:\\Users\\Igor\\Files"}
var dirsLin = []string{"/home/igor"}

func main() {
	log.Println("Starting the service ...")
	ctx := context.TODO()

	var dirs []string
	switch runtime.GOOS {
	case "windows":
		dirs = dirsWin
	case "linux":
		dirs = dirsLin
	default:
		panic("not supported OS")
	}

	w := notify.Setup(ctx, &watcher.Options{})

	for _, dir := range dirs {
		go w.StartWatching(dir, &core.WatchingOptions{
			Rescan: true,
		})
	}
	defer func() {
		for _, dir := range dirs {
			w.StopWatching(dir)
		}
	}()

	log.Println("wait for file change events ...")
	for {
		select {
		case ev := <-w.Event():
			log.Printf("[EVENT] %s: %q", watcher.ActionToString(ev.Action), ev.Path)
		case msg := <-w.Log():
			if msg.Level == "ERROR" {
				log.Printf("[%s] %s", msg.Level, msg.Message)
			}
			if msg.Level == "CRITICAL" {
				os.Exit(1)
			}
		}
	}
}
