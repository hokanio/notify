package core

import "gitlab.com/hokanio/notify/event"

type WatchingOptions struct {
	Rescan        bool
	ActionFilters []event.ActionType
}

// DirectoryWatcher interface
type DirectoryWatcher interface {
	Event() chan event.Event
	Log() chan event.Log
	RescanAll()
	StartWatching(path string, options *WatchingOptions)
	StopWatching(path string)
}
