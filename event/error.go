package event

import (
	"runtime/debug"
	"strings"
)

// Level maps debug level
type Level uint32

const (
	// UNSPECIFIED is error level
	UNSPECIFIED Level = iota
	TRACE
	DEBUG
	INFO
	WARNING
	ERROR
	CRITICAL
)

// Log type represents a log message with stacktrace, log-level and message
type Log struct {
	Stack   string
	Message string
	Level   string
}

// FormatLogMessage returns formattet error message
func FormatLogMessage(level, msg string) Log {
	stack := string(debug.Stack())

	return Log{
		Stack:   stack,
		Message: msg,
		Level:   parseLevel(level).String(),
	}
}

func parseLevel(level string) Level {
	level = strings.ToUpper(level)
	switch level {
	case "UNSPECIFIED":
		return UNSPECIFIED
	case "TRACE":
		return TRACE
	case "DEBUG":
		return DEBUG
	case "INFO":
		return INFO
	case "WARN", "WARNING":
		return WARNING
	case "ERROR":
		return ERROR
	case "CRITICAL":
		return CRITICAL
	default:
		return UNSPECIFIED
	}
}

// String implements Stringer.
func (level Level) String() string {
	switch level {
	case UNSPECIFIED:
		return "UNSPECIFIED"
	case TRACE:
		return "TRACE"
	case DEBUG:
		return "DEBUG"
	case INFO:
		return "INFO"
	case WARNING:
		return "WARNING"
	case ERROR:
		return "ERROR"
	case CRITICAL:
		return "CRITICAL"
	default:
		return "<unknown>"
	}
}
