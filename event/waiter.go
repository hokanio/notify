/*
Package event fixes the problem of multiple file change notifications for the same file from the OS API.
With FileNotificationWaiter(chanA, chanB, data) you can send data to the chanB if nothing was send to the chanA for 5 seconds

The flow:
- For each file create a channel and store it with RegisterFileNotification()
- Call FileNotificationWaiter() as a go routin with the created channel and other needed data
- On the next file change notification check if the channel for this file exists, if so send true to the channel
- If nothing was send on the channel, FileNotificationWaiter() will send the data to the provided channel after 5 seconds*/
package event

import (
	"sync"
	"time"
)

// Waiter holds event channel and error channel
type Waiter struct {
	IgnoreFiles []string
	BufferCh    chan Event
	EventCh     chan Event
	LogCh       chan Log
	Timeout     time.Duration
	MaxCount    int
}

var notificationsMutex sync.Mutex
var notificationsChans = make(map[string]chan Event)

func (w *Waiter) GetRenameHandler() string {
	return "---xxx---"
}

// RegisterFileNotification channel for a given file path, use this channel for with FileNotificationWaiter() function
func (w *Waiter) RegisterFileNotification(path string) chan Event {
	waitChan := make(chan Event)
	notificationsMutex.Lock()
	defer notificationsMutex.Unlock()
	notificationsChans[path] = waitChan
	return waitChan
}

// UnregisterFileNotification channel for a given file path
func (w *Waiter) UnregisterFileNotification(path string) {
	notificationsMutex.Lock()
	defer notificationsMutex.Unlock()
	delete(notificationsChans, path)
}

// LookupForFileNotification returns a channel for a given file path
func (w *Waiter) LookupForFileNotification(path string) (chan Event, bool) {
	notificationsMutex.Lock()
	defer notificationsMutex.Unlock()
	data, ok := notificationsChans[path]
	return data, ok
}

// Wait will send fileData to the chan stored in CallbackData after timeout
// if no signal is received on waitChan. We aggregate events events from
// the same file here.
func (w *Waiter) Wait(waitChan chan Event) {
	var fileNotificationKey string
	var event Event
	for {
		select {
		case e := <-waitChan:
			fileNotificationKey = e.Path
			switch e.Action {
			case FileRenamedOldName:
				event.OldName = e.Path
			case FileRenamedNewName:
				fileNotificationKey = w.GetRenameHandler()
				event.Path = e.Path
				event.Action = e.Action
				event.ActionName = e.ActionName
				w.EventCh <- e
				w.UnregisterFileNotification(fileNotificationKey)
				return
			default:
				event = e
			}
		case <-time.After(w.Timeout):
			// fmt.Printf("<---- %s: %s\n", event.ActionName, event.Path)
			w.EventCh <- event
			w.UnregisterFileNotification(fileNotificationKey)
			return
		}
	}
}
