package event

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func Test_parseLevel(t *testing.T) {
	tests := []struct {
		name  string
		level string
		want  Level
	}{
		{
			name:  "empty level",
			level: "",
			want:  UNSPECIFIED,
		},
		{
			name:  "UNSPECIFIED level",
			level: "UNSPECIFIED",
			want:  UNSPECIFIED,
		},
		{
			name:  "TRACE level",
			level: "TRACE",
			want:  TRACE,
		},
		{
			name:  "DEBUG level",
			level: "DEBUG",
			want:  DEBUG,
		},
		{
			name:  "INFO level",
			level: "INFO",
			want:  INFO,
		},
		{
			name:  "WARN level",
			level: "WARN",
			want:  WARNING,
		},
		{
			name:  "WARNING level",
			level: "WARNING",
			want:  WARNING,
		},
		{
			name:  "ERROR level",
			level: "ERROR",
			want:  ERROR,
		},
		{
			name:  "CRITICAL level",
			level: "CRITICAL",
			want:  CRITICAL,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got := parseLevel(tt.level)
			assert.Equal(t, tt.want, got)
		})
	}
}

func Test_parseString(t *testing.T) {
	tests := []struct {
		name  string
		level Level
		want  string
	}{
		{
			name:  "empty level",
			level: 101,
			want:  "<unknown>",
		},
		{
			name:  "UNSPECIFIED level",
			level: UNSPECIFIED,
			want:  "UNSPECIFIED",
		},
		{
			name:  "TRACE level",
			level: TRACE,
			want:  "TRACE",
		},
		{
			name:  "DEBUG level",
			level: DEBUG,
			want:  "DEBUG",
		},
		{
			name:  "INFO level",
			level: INFO,
			want:  "INFO",
		},
		{
			name:  "WARNING level",
			level: WARNING,
			want:  "WARNING",
		},
		{
			name:  "ERROR level",
			level: ERROR,
			want:  "ERROR",
		},
		{
			name:  "CRITICAL level",
			level: CRITICAL,
			want:  "CRITICAL",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got := Level(tt.level).String()
			assert.Equal(t, tt.want, got)
		})
	}
}
