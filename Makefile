BASEDIR=${CURDIR}
TMP=${BASEDIR}/tmp
LOCAL_BIN:=${TMP}/bin

install-mockgen:
	GOPATH=${TMP} go get github.com/golang/mock/gomock
	GOPATH=${TMP} go install github.com/golang/mock/mockgen

mockgen: install-mockgen
	${LOCAL_BIN}/mockgen -destination=mocks/mock_gen.go -package=mocks gitlab.com/hokanio/notify/core DirectoryWatcher

test:
	go test -timeout 30s -race -cover -p 1 ./...
	CGO_ENABLED=0 go test -timeout 30s -tags fake -v

build:
	@mkdir -p $TMP
	cd examples/notifications; go build -race -ldflags "-extldflags '-static'" -o ${TMP}/notify